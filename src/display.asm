;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Display module                    *
;=====================================

;=============================
;Macros                      *
;=============================
DISPLAY_TURNLED macro REGISTER,BITVALUE,PORT,LED
	movlw BITVALUE
        andwf REGISTER,0
        sublw BITVALUE
        btfsc STATUS,Z
        goto $+3
        bcf PORT,LED
        goto $+2
        bsf PORT,LED
	endm

;=============================
;Functions                   *
;=============================
UpdateDisplay ; Updates the display depending of the register values
	DISPLAY_TURNLED REMOTETYPE,.1,PORTA,TYPEL0 ; TYPEL0
	DISPLAY_TURNLED REMOTETYPE,.2,PORTA,TYPEL1 ; TYPEL1
	DISPLAY_TURNLED COMMAND,.1,PORTB,DLED0 ; DLED0
	DISPLAY_TURNLED COMMAND,.2,PORTB,DLED1 ; DLED1
	DISPLAY_TURNLED COMMAND,.4,PORTB,DLED2 ; DLED2
	DISPLAY_TURNLED COMMAND,.8,PORTB,DLED3 ; DLED3
	DISPLAY_TURNLED COMMAND,.16,PORTB,DLED4 ; DLED4
	DISPLAY_TURNLED COMMAND,.32,PORTB,DLED5 ; DLED5	
	return
UpdateDisplay_S ; Updates the display depending of the register values in SET mode
	DISPLAY_TURNLED REMOTETYPE,.1,PORTA,TYPEL0 ; TYPEL0
	DISPLAY_TURNLED REMOTETYPE,.2,PORTA,TYPEL1 ; TYPEL1
	DISPLAY_TURNLED REMOTEID,.1,PORTB,DLED0 ; DLED0
        DISPLAY_TURNLED REMOTEID,.2,PORTB,DLED1 ; DLED1
        DISPLAY_TURNLED REMOTEID,.4,PORTB,DLED2 ; DLED2
        DISPLAY_TURNLED REMOTEID,.8,PORTB,DLED3 ; DLED3
        DISPLAY_TURNLED REMOTEID,.16,PORTB,DLED4 ; DLED4
        DISPLAY_TURNLED REMOTEID,.32,PORTB,DLED5 ; DLED5        	
        return
