;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* PANAVOX 20KWC module              *
;=====================================

;=============================
;Functions                   *
;=============================

;General purpose functions
;=============================
PNVX_20KWC_SendHeader
        bsf PORTB,PULSE
        call Delay_500_us ; 4000 uS on
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        bcf PORTB,PULSE
        call Delay_500_us ; 4000 uS off
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        return
PNVX_20KWC_SendZeros
	bsf PORTB,PULSE ; 500 uS on
        call Delay_500_us
        bcf PORTB,PULSE ; 500 uS off
        call Delay_500_us
	decfsz GPR1,1
	goto $-5
        return
PNVX_20KWC_SendOnes
	bsf PORTB,PULSE ; 500 uS on
        call Delay_500_us
        bcf PORTB,PULSE ; 1500 uS off
	call Delay_500_us
        call Delay_500_us
        call Delay_500_us
	decfsz GPR1,1
	goto $-7
        return

;=============================
;Macros                      *
;=============================
PNVX_20KWC_SEND0 macro VALUE
        movlw VALUE
        movwf GPR1
        call PNVX_20KWC_SendZeros
        endm
PNVX_20KWC_SEND1 macro VALUE
        movlw VALUE
        movwf GPR1
        call PNVX_20KWC_SendOnes
        endm
PNVX_20KWC_CHECKCOMMAND macro VALUE,FUNCTION
	movf COMMAND,0
        sublw VALUE
        btfss STATUS,Z
        goto $+2
	call FUNCTION
	endm

;Control-specific functions
;=============================
PNVX_20KWC_0 ; Power
	call PNVX_20KWC_SendHeader
	PNVX_20KWC_SEND0 .3
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .7
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .4
	PNVX_20KWC_SEND1 .4
	PNVX_20KWC_SEND0 .8
	PNVX_20KWC_SEND1 .5
	return
PNVX_20KWC_1 ; 100
	call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .6
	PNVX_20KWC_SEND1 .3
	PNVX_20KWC_SEND0 .3
	PNVX_20KWC_SEND1 .2
	PNVX_20KWC_SEND0 .3
	PNVX_20KWC_SEND1 .4
	return
PNVX_20KWC_2 ; SLEEP
	call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .4
	PNVX_20KWC_SEND1 .2
	PNVX_20KWC_SEND0 .2
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .5
	PNVX_20KWC_SEND1 .2
	PNVX_20KWC_SEND0 .1
	PNVX_20KWC_SEND1 .4
	return
PNVX_20KWC_3 ; RECALL
	call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .4
	PNVX_20KWC_SEND1 .3
	PNVX_20KWC_SEND0 .1
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .6
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .1
	PNVX_20KWC_SEND1 .4
	return
PNVX_20KWC_4 ; VIDEO
	call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .5
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .2
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .3
	PNVX_20KWC_SEND1 .1
	PNVX_20KWC_SEND0 .1
	PNVX_20KWC_SEND1 .2
	PNVX_20KWC_SEND0 .1
	PNVX_20KWC_SEND1 .4
	return
PNVX_20KWC_5 ; 1
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .12
        PNVX_20KWC_SEND1 .9
	return
PNVX_20KWC_6 ; 2
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .8
        PNVX_20KWC_SEND1 .8
        return
PNVX_20KWC_7 ; 3
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .7
        return
PNVX_20KWC_8 ; 4
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .8
        PNVX_20KWC_SEND1 .7
        return
PNVX_20KWC_9 ; 5
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .6
        return
PNVX_20KWC_10 ; 6
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .6
        return
PNVX_20KWC_11 ; 7
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .6
        return
PNVX_20KWC_12 ; 8
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .3
        PNVX_20KWC_SEND0 .8
        PNVX_20KWC_SEND1 .6
        return
PNVX_20KWC_13 ; 9
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .3
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_14 ; 0
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_15 ; QUICKVIEW
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .8
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .4
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        return
PNVX_20KWC_16 ; MUTERESET
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .3
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_17 ; TIME
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .3
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .4
        return
PNVX_20KWC_18 ; PICTURE
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .4
        return
PNVX_20KWC_19 ; MENU
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .4
        return
PNVX_20KWC_20 ; CHANNEL-
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_21 ; CHANNEL+
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_22 ; VOLUME-
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .1
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .5
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .5
        return
PNVX_20KWC_23 ; VOLUME+
        call PNVX_20KWC_SendHeader
        PNVX_20KWC_SEND0 .3
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .7
        PNVX_20KWC_SEND1 .1
        PNVX_20KWC_SEND0 .6
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .4
        PNVX_20KWC_SEND1 .2
        PNVX_20KWC_SEND0 .2
        PNVX_20KWC_SEND1 .5
        return

;Main control routine
;=============================
PNVX_20KWC_Open
	movlw .0 ; Establish the COMMAND value
	movwf COMMAND
	movlw .23 ; Establish the MAXCOMMAND value
	movwf MAXCOMMAND
        call FreqSet38Khz ; Set the frequency to 38 Khz        
	return
PNVX_20KWC_CheckInput
	btfss PORTA,PUSHBTN ; Check if PUSH button was pushed
	btfsc SWSTATUS,2
	goto $+3
	bsf SWSTATUS,2
	call PNVX_20KWC_DoCommand
	return
PNVX_20KWC_DoCommand
	; Check the number of command and emit them
	PNVX_20KWC_CHECKCOMMAND .0,PNVX_20KWC_0 ; Power button
	PNVX_20KWC_CHECKCOMMAND .1,PNVX_20KWC_1 ; 100 button
	PNVX_20KWC_CHECKCOMMAND .2,PNVX_20KWC_2 ; Sleep button
	PNVX_20KWC_CHECKCOMMAND .3,PNVX_20KWC_3 ; Recall button
	PNVX_20KWC_CHECKCOMMAND .4,PNVX_20KWC_4 ; Video button
	PNVX_20KWC_CHECKCOMMAND .5,PNVX_20KWC_5 ; 1 button
	PNVX_20KWC_CHECKCOMMAND .6,PNVX_20KWC_6 ; 2 button
	PNVX_20KWC_CHECKCOMMAND .7,PNVX_20KWC_7 ; 3 button
	PNVX_20KWC_CHECKCOMMAND .8,PNVX_20KWC_8 ; 4 button
	PNVX_20KWC_CHECKCOMMAND .9,PNVX_20KWC_9 ; 5 button
	PNVX_20KWC_CHECKCOMMAND .10,PNVX_20KWC_10 ; 6 button
	PNVX_20KWC_CHECKCOMMAND .11,PNVX_20KWC_11 ; 7 button
	PNVX_20KWC_CHECKCOMMAND .12,PNVX_20KWC_12 ; 8 button
	PNVX_20KWC_CHECKCOMMAND .13,PNVX_20KWC_13 ; 9 button
	PNVX_20KWC_CHECKCOMMAND .14,PNVX_20KWC_14 ; 0 button
	PNVX_20KWC_CHECKCOMMAND .15,PNVX_20KWC_15 ; QuickView button
	PNVX_20KWC_CHECKCOMMAND .16,PNVX_20KWC_16 ; Mute/Reset button
	PNVX_20KWC_CHECKCOMMAND .17,PNVX_20KWC_17 ; Time button
	PNVX_20KWC_CHECKCOMMAND .18,PNVX_20KWC_18 ; Picture button
	PNVX_20KWC_CHECKCOMMAND .19,PNVX_20KWC_19 ; Menu button
	PNVX_20KWC_CHECKCOMMAND .20,PNVX_20KWC_20 ; Channel- button
	PNVX_20KWC_CHECKCOMMAND .21,PNVX_20KWC_21 ; Channel+ button
	PNVX_20KWC_CHECKCOMMAND .22,PNVX_20KWC_22 ; Volume- button
	PNVX_20KWC_CHECKCOMMAND .23,PNVX_20KWC_23 ; Volume+ button
	return
