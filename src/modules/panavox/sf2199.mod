;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* PANAVOX SF2199 module             *
;=====================================

;=============================
;Functions                   *
;=============================

;General purpose functions
;=============================
PNVX_SF2199_SendHeader
        bsf PORTB,PULSE
        call Delay_500_us ; 8000 uS on
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
	call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
	call Delay_500_us
        bcf PORTB,PULSE
        call Delay_500_us ; 4000 uS off
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        call Delay_500_us
        return
PNVX_SF2199_SendZeros
	bsf PORTB,PULSE ; 500 uS on
        call Delay_500_us
        bcf PORTB,PULSE ; 500 uS off
        call Delay_500_us
	decfsz GPR1,1
	goto $-5
        return
PNVX_SF2199_SendOnes
	bsf PORTB,PULSE ; 500 uS on
        call Delay_500_us
        bcf PORTB,PULSE ; 1500 uS off
	call Delay_500_us
        call Delay_500_us
        call Delay_500_us
	decfsz GPR1,1
	goto $-7
        return

;=============================
;Macros                      *
;=============================
PNVX_SF2199_SEND0 macro VALUE
        movlw VALUE
        movwf GPR1
        call PNVX_20KWC_SendZeros
        endm
PNVX_SF2199_SEND1 macro VALUE
        movlw VALUE
        movwf GPR1
        call PNVX_20KWC_SendOnes
        endm
PNVX_SF2199_CHECKCOMMAND macro VALUE,FUNCTION
	movf COMMAND,0
        sublw VALUE
        btfss STATUS,Z
        goto $+2
	call FUNCTION
	endm

;Control-specific functions
;=============================
PNVX_SF2199_0 ; Power
	call PNVX_SF2199_SendHeader
	PNVX_SF2199_SEND0 .6
	PNVX_SF2199_SEND1 .1
	PNVX_SF2199_SEND0 .1
	PNVX_SF2199_SEND1 .6
	PNVX_SF2199_SEND0 .1
	PNVX_SF2199_SEND1 .1
	PNVX_SF2199_SEND0 .1
	PNVX_SF2199_SEND1 .1
	PNVX_SF2199_SEND0 .2
        PNVX_SF2199_SEND1 .1
        PNVX_SF2199_SEND0 .3
        PNVX_SF2199_SEND1 .1
        PNVX_SF2199_SEND0 .1
        PNVX_SF2199_SEND1 .2
        PNVX_SF2199_SEND0 .1
        PNVX_SF2199_SEND1 .4
	return

;Main control routine
;=============================
PNVX_SF2199_Open
	movlw .0 ; Establish the COMMAND value
	movwf COMMAND
	movlw .29 ; Establish the MAXCOMMAND value
	movwf MAXCOMMAND
        call FreqSet38Khz ; Set the frequency to 38 Khz        
	return
PNVX_SF2199_CheckInput	
	btfss PORTA,PUSHBTN ; Check if PUSH button was pushed
	btfsc SWSTATUS,2
	goto $+2
	call PNVX_SF2199_DoCommand
	return
PNVX_SF2199_DoCommand
	; Check the number of command and emit them
	PNVX_SF2199_CHECKCOMMAND .0,PNVX_SF2199_0 ; Power button	
	return
