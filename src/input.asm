;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Input module                      *
;=====================================

;=============================
;Macros                      *
;=============================
INPUT_CHECK macro REGISTER,VALUE,FUNCTION,JUMPS
	movf REGISTER,0
        sublw VALUE
        btfss STATUS,Z
        goto $+JUMPS
	call FUNCTION
	return
	endm
INPUT_CHECK_BUTTON macro TYPE,SWBIT,FUNCTION
	btfss PORTA,TYPE
        btfsc SWSTATUS,SWBIT
        goto $+3
        bsf SWSTATUS,SWBIT
	call FUNCTION
	endm

;=============================
;Functions                   *
;=============================
CheckInput
	INPUT_CHECK_BUTTON SETBTN,.3,SetRemote ; Check if SET button was pressed (Go to SET mode)
	INPUT_CHECK_BUTTON PLUSBTN,.0,IncrementCommand ; Check if + button was pressed (Increment remote command)
	INPUT_CHECK_BUTTON MINUSBTN,.1,DecrementCommand ; Check if - button was pressed (Decrement remote command)
	INPUT_CHECK REMOTETYPE,.0,CheckRemote_0,3 ; Check if remote type is 0
	INPUT_CHECK REMOTETYPE,.1,CheckRemote_1,3 ; Check if remote type is 1
        INPUT_CHECK REMOTETYPE,.2,CheckRemote_2,3 ; Check if remote type is 2
        INPUT_CHECK REMOTETYPE,.3,CheckRemote_3,2 ; Check if remote type is 3        
SetRemote	
	INPUT_CHECK_BUTTON PLUSBTN,.0,IncrementRemoteID ; Check if + button was pressed (Increment remote ID)
	INPUT_CHECK_BUTTON MINUSBTN,.1,DecrementRemoteID ; Check if - button was pressed (Decrement remote ID)
	INPUT_CHECK_BUTTON PUSHBTN,.2,ChangeRemoteType ; Check if PUSH button was pressed (Change remote type)
	btfss PORTA,SETBTN ; Check if SET button was pressed
	btfsc SWSTATUS,3
        goto $+3
        bsf SWSTATUS,3
	goto $+4
	call CheckRelButtons ; Check for released buttons
	call UpdateDisplay_S ; Update display in SET mode
	goto SetRemote
	INPUT_CHECK REMOTETYPE,.0,OpenRemote_0,3 ; Check if remote type is 0        
        INPUT_CHECK REMOTETYPE,.1,OpenRemote_1,3 ; Check if remote type is 1        
        INPUT_CHECK REMOTETYPE,.2,OpenRemote_2,3 ; Check if remote type is 2        
        INPUT_CHECK REMOTETYPE,.3,OpenRemote_3,2 ; Check if remote type is 3	
CheckRelButtons
	btfsc PORTA,PLUSBTN
	bcf SWSTATUS,0
	btfsc PORTA,MINUSBTN
	bcf SWSTATUS,1
	btfsc PORTA,PUSHBTN
	bcf SWSTATUS,2
	btfsc PORTA,SETBTN
	bcf SWSTATUS,3
	return
IncrementCommand
	movf COMMAND,0
	subwf MAXCOMMAND,0
        btfss STATUS,Z
	incf COMMAND,1
        return
DecrementCommand
	movf COMMAND,0
        sublw .0
        btfss STATUS,Z
        decf COMMAND,1   
	return
IncrementRemoteID
	movf REMOTEID,0
        sublw .63
        btfss STATUS,Z
        incf REMOTEID,1
	return
DecrementRemoteID
	movf REMOTEID,0
        sublw .0
        btfss STATUS,Z
        decf REMOTEID,1
	return
ChangeRemoteType
	incf REMOTETYPE,1
	movf REMOTETYPE,0
	sublw .4
        btfss STATUS,Z
	goto $+3
	movlw 0
	movwf REMOTETYPE
	return
CheckRemote_0
	INPUT_CHECK REMOTEID,.0,PNVX_20KWC_CheckInput,3 ; Check if remote id is 0
	INPUT_CHECK REMOTEID,.1,PNVX_SF2199_CheckInput,2 ; Check if remote id is 1
CheckRemote_1 ; Not implemented for now
	return
CheckRemote_2 ; Not implemented for now
	return
CheckRemote_3 ; Not implemented for now
	return
OpenRemote_0
	INPUT_CHECK REMOTEID,.0,PNVX_20KWC_Open,3 ; Check if remote id is 0
	INPUT_CHECK REMOTEID,.1,PNVX_SF2199_Open,2 ; Check if remote id is 1
OpenRemote_1 ; Not implemented for now
	return
OpenRemote_2 ; Not implemented for now
	return
OpenRemote_3 ; Not implemented for now
	return
