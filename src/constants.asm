;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Constants module                  *
;=====================================

;=============================
;Constants                   *
;=============================

;Remote control specific
;=============================
REMOTETYPE EQU 0x27
REMOTEID EQU 0x28
COMMAND EQU 0x29
MAXCOMMAND EQU 0x2A
SWSTATUS EQU 0x2B
GPR1 EQU 0x2C

;Remote control pin specific (PORTA)
;=============================
ONLED EQU 0
PUSHBTN EQU 1
TYPEL0 EQU 2
TYPEL1 EQU 3
SETBTN EQU 4
RESET EQU 5
PLUSBTN EQU 6
MINUSBTN EQU 7

;Remote control pin specific (PORTB)
;=============================
DLED0 EQU 0
DLED1 EQU 1
DLED2 EQU 2
CARRIER EQU 3
PULSE EQU 4
DLED5 EQU 5
DLED4 EQU 6
DLED3 EQU 7

