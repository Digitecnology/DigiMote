;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Timers module                     *
;=====================================

;=============================
;Functions                   *
;=============================
Delay_500_us
        movlw .163 ; 1
        movwf DELAY1 ; 1
        call delay1
        return ; 4
