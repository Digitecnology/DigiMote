;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Main application                  *
;=====================================

;=============================
;General description         *
;=============================
;
; RA0/17 => ON/OFF LED
; RA1/18 => CHANGE INPUT SWITCH
; RA2/1 => SET SWITCH
; RA3/2 => TYPE LED 0
; RA4/3 => TYPE LED 0
; RA5/4 => RESET
; RA6/15 => INPUT SWITCH +
; RA7/16 => INPUT SWITCH -
; RB0/6 => DISPLAY LED 0
; RB1/7 => DISPLAY LED 1
; RB2/8 => DISPLAY LED 2
; RB3/9 => PWM OUTPUT (BASE FREQUENCY) (38 KHz)
; RB4/10 => ELECTRONIC TICKS (ADD/REMOVE FREQUENCY)
; RB5/11 => DISPLAY LED 5
; RB6/12 => DISPLAY LED 4
; RB7/13 => DISPLAY LED 3
; VDD/14 => +5V INPUT
; VSS/5 => GROUND
; CONSTANT SWSTATUS => 0000SP-+
;
; REMOTETYPE VALUES:
;  0 = TV
;  1 = AIR CONDITIONING
;  3 = MINICOMPONENT
;  4 = MISCELLANOUS
; REMOTEID VALUES (TV):
;  0 = PANAVOX 20KWC

;=============================
;Assembler registers         *
;=============================
	include constants.asm

;=============================
;Assembler directives        *
;=============================
	;Specify PIC type, configuration and header
        list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
	org 0x00
	goto Start

;=============================
;Functions                   *
;=============================
	; Digitecnology library
	include digitecnology.inc
	; System modules
	include timers.asm
	include display.asm
	include frequency.asm
	include input.asm
	; Remote control modules
	include modules/panavox/20kwc.mod
	include modules/panavox/sf2199.mod
SetDefaults
	; Set default values of the remote control
	movlw .0
	movwf REMOTETYPE
	movwf REMOTEID
	movwf COMMAND
	movwf MAXCOMMAND
	movwf SWSTATUS	
	; Open the first remote control as the default
	call OpenRemote_0	
	return

;=============================
;Program                     *
;=============================
Start
	goto Initialize
Initialize
	; Turn off the comparator and the voltage reference modules
	movlw 0x07
	movwf CMCON
	bcf VRCON,7
	bcf VRCON,6
	; Configure the pins as inputs/outputs
	call chgbnk1 ; Go to bank 1	
	bcf TRISA,ONLED ; Output
	bsf TRISA,PUSHBTN ; Input
	bcf TRISA,TYPEL0 ; Output
	bcf TRISA,TYPEL1 ; Output
	bsf TRISA,SETBTN ; Input
	bsf TRISA,PLUSBTN ; Input
	bsf TRISA,MINUSBTN ; Input
	bcf TRISB,DLED0 ; Output
	bcf TRISB,DLED1 ; Output
	bcf TRISB,DLED2 ; Output
	bcf TRISB,CARRIER ; Output
	bcf TRISB,PULSE ; Output
	bcf TRISB,DLED5 ; Output
	bcf TRISB,DLED4 ; Output
	bcf TRISB,DLED3 ; Output
	call chgbnk0 ; Go to bank 0
	; Turn on the ON led and set the default values
	bsf PORTA,ONLED
	call SetDefaults
	; Go to Main Loop
	goto MainLoop
MainLoop
	call CheckInput ; Check for input
	call CheckRelButtons ; Check for released buttons
	call UpdateDisplay ; Update the display	
	goto MainLoop
	end
