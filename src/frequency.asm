;   DigiMote - An Universal remote control for generic purpose use
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of DigiMote.
;
;   DigiMote is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;=====================================
;*DigiMote (2009 - Steven Rodriguez) *
;*                                   *
;* Frequency module                  *
;=====================================

;=============================
;Functions                   *
;=============================
FreqSet38Khz
	; Initialize the base frequecy generator (PWM)
        call chgbnk1 ; Go to bank 1
        movlw b'00011001' ; PWM period
        movwf PR2
        call chgbnk0 ; Go to bank 0
        movlw b'00001101' ; PWM Duty cycle
        movwf CCPR1L        
        call chgbnk1 ; Go to bank 1
        bcf TRISB,CARRIER ; Set CCP1 to output
        call chgbnk0 ; Go to bank 0
        movlw b'00000100' ; Set prescale value to 1 and the timer on
        movwf T2CON
        movlw b'00001100' ; Set PWM on
        movwf CCP1CON
	return
FreqSet56Khz
	return
FreqSetOff
	return
